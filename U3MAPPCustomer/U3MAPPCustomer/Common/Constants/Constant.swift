//
//  Constant.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 10/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

let APPLICATION_NAME = Bundle.main.infoDictionary!["CFBundleName"] as! String
let APPDELEGATE: AppDelegate = UIApplication.shared.delegate as! AppDelegate

// App Button Hex Color Code
let firstColorHex = "#9BEEFF"
let secondColorHex = "#E5F7FF"
let thirdColorHex = "#EDF9FF"
let forthColorHex = "#EAFFE0"
let skyColorHex = "#2DCBEB"
let grayColorHex = "6D6D6D"

let rFirstCgColor = UIColor(hexString: "#A9EFFF")
let rSecondCgColor = UIColor(hexString: "#CAE1F8")
let rThirdCgColor = UIColor(hexString: "#EDFFDF")

//App Button Hex UIColor
let firstCgColor = UIColor(hexString: firstColorHex)
let secondCgColor = UIColor(hexString: secondColorHex)
let thirdCgColor = UIColor(hexString: thirdColorHex)
let forthCgColor = UIColor(hexString: forthColorHex)
let skyCgColor = UIColor(hexString: skyColorHex)
let shadowGrayColor = UIColor(hexString: grayColorHex, alpha: 0.2)

struct Storyboards {
    internal static let homeStoryboard        =  UIStoryboard(name: "Main", bundle: nil)
    internal static let loginStoryboard       =  UIStoryboard(name: "Login", bundle: nil)
}

struct StoryboardIdentifiers {
    internal static let dashboardNavigation         =  "DashboardNavigation"
    internal static let loginNavigation             =  "LoginNavigation"
    internal static let completeProfileNavigation   =  "CompleteProfileNavigation"
}



//MARK:- API Methods.
enum APIMethods: String {
    case signup                               = "Account/SignUp"
    case agentLogin                           = "Account/U3AgentLogin"
    case getCustomerProfile                   = "Customer/GetCustomerProfile"
    case updateProfile                        = "Customer/UpdateProfile"
    case redeemFreebox                        = "Customer/RedeemFreebox"
    case masterCheckList                      = "Customer/GetMasterCheckList"
    case myCheckList                          = "Customer/GetMyChecklist"
    case changeChecklistItemStatus            = "Customer/ChangeChecklistItemStatus"
    case addMyCheckList                       = "Customer/AddMyCheckList"
    case searchNearbyDistributor              = "Customer/SearchNearbyDistributor"
    case logout                               = "Account/UserLogout"
    case qrCodeValidate                       = "Distributer/ValidateQRCode"
    case allotBoxes                           = "Distributer/AllotBoxes"
}

//MARK:-API Headers
enum apiHeaderKeys : String,CaseIterable {
    case contentTypeKey                       = "Content-Type"
    case apiServiceToken                      = "ApiServiceToken"
    case offsetKey                            = "Offset"
    case deviceType                           = "DeviceType"
    case authTokenKey                         = "AuthorizationToken"
    case useridKey                            = "UserId"

}

enum apiHeaderValues : String,CaseIterable {
    case contentTypeValue                     = "application/json"
    case apiServiceTokenValue                 = "U3System@2020"
    case offsetValue                          = "1"
}


enum Images {
    case downArrow
    case profilePlaceholder
    case backArrow
    case menu
    case location
    case edit
    case plus
    
    var icon: UIImage {
        switch self {
        case .downArrow: return UIImage(named: "down_arrowd_icon") ?? UIImage()
        case .profilePlaceholder: return UIImage(named: "user") ?? UIImage()
        case .backArrow: return UIImage(named: "back_arrow")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .menu: return UIImage(named: "menuIC")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .location: return UIImage(named: "map_pin")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .edit: return UIImage(named: "edit_list")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .plus: return UIImage(named: "add_header_icon")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        }
    }
}

enum gender : Int {
    case male = 1
    case female = 2
    case unknown = 0
}
