//
//  Utility.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static func lastController() -> UIViewController {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return UIViewController()
    }
    
    static  func checkNullString(string : AnyObject?) -> String{
        if string != nil {
            return string as! String
        } else {
            return ""
        }
    }
    
    
    static func ValidationError(errorMessage : String, controller : AnyObject){
        let alert = UIAlertController(title: APPLICATION_NAME, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)

    }
    
    static func convertDateFromCurrentFormatToGivenFormat(currentformat : String, givenFormat :String, convertibleDate : String ) -> String{
        
        let dateFormatter : DateFormatter =  DateFormatter()
        dateFormatter.dateFormat = currentformat
        let date  = dateFormatter.date(from: convertibleDate)
        
        dateFormatter.dateFormat = givenFormat
        let strNewDate : String = dateFormatter.string(from: date!)
        return strNewDate
    }
        
    static func convertDateFormater(date: String, formatter: String = "MM/dd/yyyy hh:mm:ss a") -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        guard let date = dateFormatter.date(from: date) else { return "" }
        dateFormatter.dateFormat = "dd MMM yyyy"
        return  dateFormatter.string(from: date)

    }
    
    static func getStingFromImg(image :UIImage) -> String{
        let image : UIImage = image
        let imageData:Data = image.jpegData(compressionQuality: 0.2)!
        let strBase64:String = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64 as String
    }
    
    static func changeStringColor(string_to_color: String, main_string: String, colorHexCode : String)-> NSMutableAttributedString {

        let range = (main_string as NSString).range(of: string_to_color)

        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: colorHexCode) , range: range)


        return attribute
    }
}
