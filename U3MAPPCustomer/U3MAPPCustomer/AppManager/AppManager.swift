//
//  AppManager.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AppManager: NSObject {

    
    // Can't init is singleton
    private override init() { }
    
    // MARK: Shared Instance
    
    static let shared: AppManager = {
        return AppManager()
    }()
    
    
    var isLoggedIn : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isLoggedIn")
            UserDefaults.standard.synchronize()
        }
        get{
            if let userloggedIn = UserDefaults.standard.object(forKey: "isLoggedIn")  as? Bool {
                return userloggedIn
            }
            else{
                return false
            }
        }
    }
    
    //MARK:-User variables
    var userModel : UserModel{
        set{
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: newValue)
            userDefaults.set(encodedData, forKey: "UserModel")
            userDefaults.synchronize()
        }
        get{
            if let decoded  = UserDefaults.standard.object(forKey: "UserModel") as? Data{
                let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserModel
                return decodedUser
            }else{
                let dict = ["APIToken":"dsdasddsad", "IsProfileComplete":false,"UserId":0] as [String : Any]
                return UserModel.init(fromDictionary: dict)
            }
        }
    }
    
    var userModelAgent : UserModelAgent{
           set{
               let userDefaults = UserDefaults.standard
               let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: newValue)
               userDefaults.set(encodedData, forKey: "UserModelAgent")
               userDefaults.synchronize()
           }
           get{
               if let decoded  = UserDefaults.standard.object(forKey: "UserModelAgent") as? Data{
                   let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserModelAgent
                   return decodedUser
               }else{
                   let dict = ["agentName":"dsdasddsad", "authToken":"","userId":0] as [String : Any]
                   return UserModelAgent.init(fromDictionary: dict)
               }
           }
       }
    
    
    
    var isRememberMe : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isRememberMe")
            UserDefaults.standard.synchronize()
        }
        get{
            if let rememberMe = UserDefaults.standard.object(forKey: "isRememberMe")  as? Bool {
                return rememberMe
            }
            else{
                return false
            }
        }
    }
    
    var isProfileCompleted : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isProfileCompleted")
            UserDefaults.standard.synchronize()
        }
        get{
            if let isProfileCompleted = UserDefaults.standard.object(forKey: "isProfileCompleted")  as? Bool {
                return isProfileCompleted
            }
            else{
                return false
            }
        }
    }
    
    
    var email : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "email")
            UserDefaults.standard.synchronize()
        }
        get{
            if let code = UserDefaults.standard.value(forKey: "email") as? String {
                return code
            } else {
                return ""
            }
        }
    }
    
    var password : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "password")
            UserDefaults.standard.synchronize()
        }
        get{
            if let code = UserDefaults.standard.value(forKey: "password") as? String {
                return code
            } else {
                return ""
            }
        }
    }
    


    //MARK:-PushNotification variables
    
    var deviceToken : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "deviceToken")
            UserDefaults.standard.synchronize()
        }
        get{
            if let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as? String {
                return deviceToken
            } else {
                return "tokenNotFound"
            }
        }
    }
    
    open func showRootView() {
        
        APPDELEGATE.showLoginWindow()
    }
  
    
    
    //MARK:-Utility Methods
    
    
    func getHeaders() -> [String:String] {
        var headers : [String:String] = [:]
        headers[apiHeaderKeys.contentTypeKey.rawValue] = apiHeaderValues.contentTypeValue.rawValue
        headers[apiHeaderKeys.apiServiceToken.rawValue] = apiHeaderValues.apiServiceTokenValue.rawValue
        headers[apiHeaderKeys.offsetKey.rawValue] = apiHeaderValues.offsetValue.rawValue
       
        return headers
    }
    
    func getLoggedInHeaders() -> [String:String] {
        var headers : [String:String] = [:]
        headers[apiHeaderKeys.contentTypeKey.rawValue] = apiHeaderValues.contentTypeValue.rawValue
        headers[apiHeaderKeys.apiServiceToken.rawValue] = apiHeaderValues.apiServiceTokenValue.rawValue
        headers[apiHeaderKeys.offsetKey.rawValue] = apiHeaderValues.offsetValue.rawValue
        headers[apiHeaderKeys.authTokenKey.rawValue] = AppManager.shared.userModelAgent.authToken
        headers[apiHeaderKeys.useridKey.rawValue] = "\(AppManager.shared.userModelAgent.userId!)"
        return headers
    }
    
    
    
    
    func timeStampToString(timStamp : Int) -> String {
        let dateTimeStamp =  Date.init(timeIntervalSince1970: Double(timStamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd yyyy"
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
    }
   
    
    func openSettings(controller : UIViewController)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Unable to use notifications",
                                          message: "To enable notifications, go to Settings and enable notifications for this app.",
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
                // Take the user to Settings app to possibly change permission.
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
            })
            alert.addAction(settingsAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Redirection on QR code scan screen
    func  redirectionOnQRCodeScan(viewController : UIViewController)  {
        let qrScannerVC = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "QRScannerVC") as! QRScannerVC
        viewController.navigationController?.pushViewController(qrScannerVC, animated: true)
    }
    
    
}
