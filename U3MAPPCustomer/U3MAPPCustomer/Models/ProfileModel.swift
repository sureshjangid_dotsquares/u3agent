//
//  ProfileModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class ProfileModel : NSObject, NSCoding{

    var address : String!
    var age : Int!
    var contact : String!
    var customerId : Int!
    var customerName : String!
    var email : String!
    var gender : Int!
    var logoProfilePic : String!


    override init() {
        
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        age = dictionary["age"] as? Int
        contact = dictionary["contact"] as? String
        customerId = dictionary["customerId"] as? Int
        customerName = dictionary["customerName"] as? String
        email = dictionary["email"] as? String
        gender = dictionary["gender"] as? Int
        logoProfilePic = dictionary["logoProfilePic"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if age != nil{
            dictionary["age"] = age
        }
        if contact != nil{
            dictionary["contact"] = contact
        }
        if customerId != nil{
            dictionary["customerId"] = customerId
        }
        if customerName != nil{
            dictionary["customerName"] = customerName
        }
        if email != nil{
            dictionary["email"] = email
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if logoProfilePic != nil{
            dictionary["logoProfilePic"] = logoProfilePic
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address = aDecoder.decodeObject(forKey: "address") as? String
         age = aDecoder.decodeObject(forKey: "age") as? Int
         contact = aDecoder.decodeObject(forKey: "contact") as? String
         customerId = aDecoder.decodeObject(forKey: "customerId") as? Int
         customerName = aDecoder.decodeObject(forKey: "customerName") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         gender = aDecoder.decodeObject(forKey: "gender") as? Int
         logoProfilePic = aDecoder.decodeObject(forKey: "logoProfilePic") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if age != nil{
            aCoder.encode(age, forKey: "age")
        }
        if contact != nil{
            aCoder.encode(contact, forKey: "contact")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customerId")
        }
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customerName")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if logoProfilePic != nil{
            aCoder.encode(logoProfilePic, forKey: "logoProfilePic")
        }

    }

}
