//
//  UserModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class UserModel: NSObject , NSCoding{

    var authToken : String!
    var customerName : String!
    var email : String!
    var id : Int!
    var isProfileCompeled : Int!
    var logoProfilePic : String!
    var phoneNumber : String!

    
    

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        authToken = dictionary["authToken"] as? String
        customerName = dictionary["customerName"] as? String
        email = dictionary["email"] as? String
        id = dictionary["id"] as? Int
        isProfileCompeled = dictionary["isProfileCompeled"] as? Int
        logoProfilePic = dictionary["logoProfilePic"] as? String
        phoneNumber = dictionary["phoneNumber"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if authToken != nil{
            dictionary["authToken"] = authToken
        }
        if customerName != nil{
            dictionary["customerName"] = customerName
        }
        if email != nil{
            dictionary["email"] = email
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isProfileCompeled != nil{
            dictionary["isProfileCompeled"] = isProfileCompeled
        }
        if logoProfilePic != nil{
            dictionary["logoProfilePic"] = logoProfilePic
        }
        if phoneNumber != nil{
            dictionary["phoneNumber"] = phoneNumber
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         authToken = aDecoder.decodeObject(forKey: "authToken") as? String
         customerName = aDecoder.decodeObject(forKey: "customerName") as? String
         email = aDecoder.decodeObject(forKey: "email") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         isProfileCompeled = aDecoder.decodeObject(forKey: "isProfileCompeled") as? Int
         logoProfilePic = aDecoder.decodeObject(forKey: "logoProfilePic") as? String
         phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if authToken != nil{
            aCoder.encode(authToken, forKey: "authToken")
        }
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customerName")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isProfileCompeled != nil{
            aCoder.encode(isProfileCompeled, forKey: "isProfileCompeled")
        }
        if logoProfilePic != nil{
            aCoder.encode(logoProfilePic, forKey: "logoProfilePic")
        }
        if phoneNumber != nil{
            aCoder.encode(phoneNumber, forKey: "phoneNumber")
        }

    }

}
