//
//	Data.swift
//
//	Create by Abhishek Jangid on 6/3/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserModelAgent : NSObject, NSCoding{

	var agentName : String!
	var authToken : String!
	var userId : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		agentName = dictionary["agentName"] as? String
		authToken = dictionary["authToken"] as? String
		userId = dictionary["userId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if agentName != nil{
			dictionary["agentName"] = agentName
		}
		if authToken != nil{
			dictionary["authToken"] = authToken
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agentName = aDecoder.decodeObject(forKey: "agentName") as? String
         authToken = aDecoder.decodeObject(forKey: "authToken") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if agentName != nil{
			aCoder.encode(agentName, forKey: "agentName")
		}
		if authToken != nil{
			aCoder.encode(authToken, forKey: "authToken")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}

	}

}
