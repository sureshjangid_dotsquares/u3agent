//
//  CongratulationVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CongratulationVC: UIViewController {

    //MARK:- IBOutltes
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txt: UITextView!{
        didSet {
            txt.text = msg
        }
    }
    
    
    var msg: String = ""
    var callback: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewMain.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner], radius: 25)
    }
    
    @IBAction func actionOk(_ sender: UIButton) {
        
        self.dismiss(animated: false) {
            self.callback?()
            
        }
    }

}
