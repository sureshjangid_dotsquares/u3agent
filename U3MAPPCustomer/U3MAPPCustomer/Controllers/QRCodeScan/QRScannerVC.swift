//
//  QRScannerVC.swift
//  Yalgo
//
//  Created by vinod k on 23/05/19.
//  Copyright © 2019 Tushar Arora. All rights reserved.
//

import UIKit
import AVFoundation


class QRScannerVC: UIViewController {
    
    //MARK:-Outlets
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var textFieldQRCode: UITextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    
    //MARK:-Variables
    private var captureSession: AVCaptureSession!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    //MARK:-Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.title = "Scan code"
        textFieldQRCode.delegate = self
        
        navigationItem.hidesBackButton = true
        
        
        
        buttonSubmit.setTitle("SUBMIT", for:.normal)
        buttonSubmit.setTitleColor(UIColor.white, for:.normal)
        buttonSubmit.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        buttonSubmit.layer.shadowOffset = CGSize(width:0, height:5)
        buttonSubmit.layer.shadowOpacity = 1.0
        buttonSubmit.layer.shadowRadius = 5
        buttonSubmit.layer.cornerRadius = 5.0
        buttonSubmit.backgroundColor = UIColor(hexString:"#2DCBEB")
        
        captureSession = AVCaptureSession()
        
        textFieldQRCode.text = ""
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            
        }else {
            AVCaptureDevice.requestAccess(for: .video) { (granted:Bool) in
                if granted == false {
                    // User rejected
                    self.presentAlertWith(message: "Please allow camera access from settings.", oKTitle: "Ok", okaction: {
                        
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                        
                    }) {}
                    
                }
            }
        }
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        // Initialize the captureSession object.
        captureSession = AVCaptureSession()
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = viewContainer.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        viewContainer.layer.addSublayer(previewLayer)
        
        // Start video capture.
        captureSession?.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopScanning()
    }
    
    private func startScanning(){
        DispatchQueue.main.async {
            guard let _captureSession = self.captureSession else {return}
            _captureSession.startRunning()
        }
        
    }
    
    private func stopScanning() {
        DispatchQueue.main.async {
            guard let _captureSession = self.captureSession else {return}
            _captureSession.stopRunning()
        }
        
    }
    
    
    
    private func goToAllotBoxScreen(code:String, userId: Int) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:"AllotBoxesViewController") as? AllotBoxesViewController {
            vc.alphanuericCode = code
            vc.userId = userId
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        guard let code = self.textFieldQRCode.text, !code.isEmpty else {
            return
        }
        self.validateQR(userId: 0, code: code)
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func failed() {
        //        let ac = UIAlertController(title: "Scanning not supported", message: Constant.ValidationMessages.qrFailed, preferredStyle: .alert)
        //        ac.addAction(UIAlertAction(title: "OK", style: .default))
        //        present(ac, animated: true)
        captureSession = nil
    }
    
    func addQrCodeImage() {
        let image = UIImage(named: "qrCode")?.cgImage
        let myLayer = CALayer()
        myLayer.frame = CGRect(origin:CGPoint.zero, size: CGSize(width: 100, height: 100))
        myLayer.contents = image
        myLayer.position = view.center
        
        viewContainer.layer.insertSublayer(myLayer, above: previewLayer)
        viewContainer.layer.addSublayer(myLayer)
    }
    
    public func ValidateString(stringValue: String) -> (Bool, String) {
//        let str = "3UAPP_AC4D5T_2_95"
        let array = stringValue.components(separatedBy: "_")
        
        guard array.count > 0 else {
            return (false, "0")
            
        }
        guard array[0] == "3UAPP" && array[1] == "AC4D5T" else {
            return (false, "0")
            
        }
        if array[2] == "1" {
            //Customer
            print("userid\(array[3])")
            
            return (true, array[3])
            
        }else if array[2] == "2" {
            //Advertiser
            print("advertise here")
            
            return (false, array[3])
        }
        return (false, "0")
    }
    
}

extension QRScannerVC : AVCaptureMetadataOutputObjectsDelegate{
    //MARK: - AVCaptureMetadataOutputObjectsDelegate -
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            return
        }
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            stopScanning()
            
            let StringData = self.ValidateString(stringValue: stringValue)
            
            if StringData.0 {
                self.validateQR(userId: Int(StringData.1)!, code: "")
            }else {
                self.presentAlertWith(message: "QR Code Validation Error")
            }
        }
        
        dismiss(animated: true)
    }
}


extension QRScannerVC: UITextFieldDelegate {
    //MARK: - UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        stopScanning()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        startScanning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 8
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

//MARK: Webservice CALL
extension QRScannerVC {
    private func validateQR(userId:Int,code:String){
        
        let params : [String:Any] = [
            "UserId": userId,
            "AlphanuericCode": code,
        ]
        
        showHud()
        WebServiceHelper.request(path: .qrCodeValidate, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                
                self.textFieldQRCode.text = ""
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let check = rootModel.data as? Bool {
                                if check {
                                    self.goToAllotBoxScreen(code: code, userId: userId)
                                }else{
                                    self.presentAlertWith(message: rootModel.successMsg)
                                }
                            }
                        }else if rootModel.responseCode == ErrorCode.resultError.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0]) {
                                    self.startScanning()
                                }
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg) {
                                self.startScanning()
                            }
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.") {
                            self.startScanning()
                        }
                    }
                }
                
            }
        }
    }
}
