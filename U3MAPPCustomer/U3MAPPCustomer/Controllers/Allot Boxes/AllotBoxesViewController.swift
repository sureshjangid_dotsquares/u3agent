//
//  AllotBoxesViewController.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 06/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AllotBoxesViewController: U3BaseViewController {
    
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var customerName: UILabel!
    
    
    private var boxes:[AllotBox] = []
    
    var alphanuericCode:String = ""
    var userId:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.backBarBtnPressed()
        
        
        self.title = "Allot Boxes"
        
        self.view.backgroundColor = UIColor.init(hexString:"#F9F9F9")
        
        buttonSubmit.setTitle("SUBMIT", for:.normal)
        buttonSubmit.setTitleColor(UIColor.white, for:.normal)
        buttonSubmit.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        buttonSubmit.layer.shadowOffset = CGSize(width:0, height:5)
        buttonSubmit.layer.shadowOpacity = 1.0
        buttonSubmit.layer.shadowRadius = 5
        buttonSubmit.layer.cornerRadius = 5.0
        buttonSubmit.backgroundColor = UIColor(hexString:"#2DCBEB")
        
        customerName.text = "Customer Name"
        
        containerView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.05).cgColor
        containerView.layer.shadowOffset = CGSize(width:0, height:9)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 10
        
        
        for index in 1...10 {
            boxes.append(AllotBox.init(boxSequence:"Box-\(index) ID", boxCode:nil))
        }
        
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60.0
        tableView.dataSource = self
        tableView.delegate = self
        
        
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        
        let filterArray = boxes.filter{$0.boxCode.trimString().isEmpty == false}
        if filterArray.count == 0 {
            self.presentAlertWith(message: "Please fill atleast 1 box id to submit")
            return
        }
        self.validateAllotBox()
        
    }
    
}

extension AllotBoxesViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.boxes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let box_cell = tableView.dequeueReusableCell(withIdentifier:AllotBoxTableViewCell.dequeIdentifier, for:indexPath) as! AllotBoxTableViewCell
        box_cell.boxSequenceNumber.text = boxes[indexPath.row].boxSequence
        box_cell.boxCodeTextField.text = boxes[indexPath.row].boxCode
        box_cell.boxCodeTextField.placeholder =  boxes[indexPath.row].boxCode
        box_cell.boxCodeTextField.addTarget(self, action:#selector(boxTextFieldEditingChanged(_:)), for:.editingChanged)
        box_cell.boxCodeTextField.indexPath = indexPath
        return box_cell
    }
    
    @objc func boxTextFieldEditingChanged(_ boxTextField:BoxTextField){
        guard let editedIndexPath = boxTextField.indexPath else {return}
        boxes[editedIndexPath.row].boxCode = boxTextField.text!
        print("Box number updated ; \(editedIndexPath.row) : \(boxTextField.text!)")
    }
}

//MARK: UITextfieldDelegate Method
extension AllotBoxesViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}


//MARK: Webservice CALL
extension AllotBoxesViewController {
    private func validateAllotBox(){
        
        var boxIds:[String] = []
        
        for item in boxes {
            boxIds.append(item.boxCode)
        }
        
        let params : [String:Any] = [
            "UserId": userId,
            "AlphanuericCode": alphanuericCode,
            "BoxIds":boxIds
        ]
        
        showHud()
        WebServiceHelper.request(path: .allotBoxes, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let vc = self.storyboard?.instantiateViewController(withIdentifier:"CongratulationVC") as? CongratulationVC {
                                vc.msg = rootModel.successMsg
                                vc.callback = {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                self.present(vc, animated: true)
                                
                            }
                        }else if rootModel.responseCode == ErrorCode.resultError.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.")
                    }
                }
                
            }
        }
    }
}
