//
//  AllotBox.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 06/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AllotBox: NSObject {
    
    private(set) var boxSequence:String!
    var boxCode:String = ""
    
    init(boxSequence:String,boxCode:String?) {
        self.boxSequence = boxSequence
        self.boxCode = boxCode ?? ""
    }
    
    private override init() {}
}
