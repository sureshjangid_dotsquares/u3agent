//
//  AllotBoxTableViewCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 06/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class BoxTextField:UITextField {
    var indexPath:IndexPath?
}

class AllotBoxTableViewCell: UITableViewCell {
    
    
    static let dequeIdentifier = "allotBoxesCellIdentifier"
    
    @IBOutlet weak var boxSequenceNumber: UILabel!
    @IBOutlet weak var boxCodeTextField: BoxTextField!
    
    
    
}
