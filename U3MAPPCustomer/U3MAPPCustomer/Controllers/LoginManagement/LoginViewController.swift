//
//  LoginViewController.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 09/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class LoginViewController: U3BaseViewController {

    //MARK:- IBOutlets declaration
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var passwordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var lblTitle:UILabel!
        
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.setGradientColor()
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        
        lblTitle.attributedText = Utility.changeStringColor(string_to_color: "3U Agents", main_string: "Welcome 3U Agents", colorHexCode: "#147D92")
        
        self.passwordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.passwordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.passwordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.passwordTxtFld.isSecureTextEntry = true
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        clearInformation()
    }
    
    func clearInformation() {
        emailTxtFld.text = ""
        passwordTxtFld.text = ""
//        emailTxtFld.text = "agent1@ds.com"
//        passwordTxtFld.text = "123456"
    }
    
    //MARK:- IBAction
    //Login Btn Press
    @IBAction func didTapLoginBtn(_ sender : UIButton){
        if self.ValidateEntries() == false {
            return
        }
        
        wsLoginUser()
    }
    

}


extension LoginViewController {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if emailTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email")
            return false
        }
        else if emailTxtFld.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email", controller: self)
            return false
        }
        else if passwordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter password")
            return false
        }
        
        return true
    }
}


//MARK:-API Calling
extension LoginViewController{
    func wsLoginUser(){
        let params : [String:Any] = [
            "Email":emailTxtFld.text?.trimString() ?? "",
            "Password":passwordTxtFld.text?.trimString() ?? "",
            "DeviceToken":AppManager.shared.deviceToken,
            ]
        
        showHud()
        WebServiceHelper.request(path: .agentLogin, method: .post, token: nil, headers: AppManager.shared.getHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                self.clearInformation()
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                
                                
                                //let userModel = UserModel.init(fromDictionary: dictResponse)
                                let userModel = UserModelAgent.init(fromDictionary: dictResponse)
                                
                                AppManager.shared.userModelAgent = userModel
                                AppManager.shared.isLoggedIn = true
                                
                                AppManager.shared.isRememberMe = true
                                AppManager.shared.email = self.emailTxtFld.text?.trimString() ?? ""
                                AppManager.shared.password = self.passwordTxtFld.text?.trimString() ?? ""
                                
                                 AppManager.shared.redirectionOnQRCodeScan(viewController: self)
                                

                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.")
                    }
                }
                
            }
        }
    }
    
    
}

