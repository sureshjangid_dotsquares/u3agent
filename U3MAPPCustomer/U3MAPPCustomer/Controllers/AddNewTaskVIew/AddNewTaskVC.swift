//
//  AddNewTaskVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import GrowingTextView

class AddNewTaskVC: UIViewController,UITextViewDelegate {
    
    //MARK:- IBOutltes
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var textView: GrowingTextView!
    
    var  taskText: ((String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewMain.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner], radius: 25)
                
        
    }
    
    @IBAction func didTapOnView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        if textView.text.trimString().isEmpty {
            self.presentAlertWith(message: "Please enter text here")
            return
        }
        
        self.dismiss(animated: false) {
            self.taskText?(self.textView.text)
            
        }
    }
    
    
}
