//
//  U3NavigationController.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class U3NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        hideNavBottomLine()
    }
    
    public func clearNavBarColor() {
        // Make the navigation bar background clear
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }
    
    public func hideNavBottomLine() {
        navigationBar.hideBottomHairline()
    }
    
    public func showBottomHairline() {
        navigationBar.showBottomHairline()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
