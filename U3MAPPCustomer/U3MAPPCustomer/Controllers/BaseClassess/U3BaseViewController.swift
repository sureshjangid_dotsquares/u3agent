//
//  U3BaseViewController.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import SWRevealViewController

class U3BaseViewController: UIViewController,SWRevealViewControllerDelegate {

    // MARK:  ViewController lifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
//    public func setGradientColor(colors:[UIColor] = [firstCgColor,secondCgColor,thirdCgColor,forthCgColor], orientaion:GradientOrientation = .vertical) {
//        self.view.applyGradient(with: colors, gradient: orientaion)
//    }
    
    public func setGradientColor(colors:[CGColor] = [firstCgColor.cgColor,secondCgColor.cgColor,thirdCgColor.cgColor,forthCgColor.cgColor], angle: Float = 0) {
        self.view.setGradient(colors: colors, angle: angle)
    }
    
    
    
    
    public func setupMenuBtn() {
        
        let revealViewController = self.revealViewController()
        revealViewController?.delegate = self
        
        if (( revealViewController ) != nil) {
            
            let sideBarBtn : UIBarButtonItem = UIBarButtonItem(image: Images.menu.icon, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            self.navigationItem.leftBarButtonItem = sideBarBtn
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    // MARK:  Back Button Method
    public func backBarBtnPressed()
    {
        let backBarBtn : UIBarButtonItem = UIBarButtonItem(image: Images.backArrow.icon, style: .plain, target: self, action: #selector(self.backBtnAction(_:)))
        self.navigationItem.leftBarButtonItem = backBarBtn
    }
    
    // MARK:  Back Button Method
    public func rightBarBtnPressed(img:UIImage = Images.edit.icon)
    {
        let rightBarBtn : UIBarButtonItem = UIBarButtonItem(image: img, style: .plain, target: self, action: #selector(self.rightBtnAction(_:)))
        self.navigationItem.rightBarButtonItem = rightBarBtn
    }
    
    @objc func rightBtnAction(_ sender: UIBarButtonItem){
        print("right button pressed")
    }
    
    @objc func backBtnAction(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Reveal View Delegate Methods
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if position == FrontViewPosition.left {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.view.isUserInteractionEnabled = true
            
            
        } else {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.view.isUserInteractionEnabled = false
        }
    }
    

}
