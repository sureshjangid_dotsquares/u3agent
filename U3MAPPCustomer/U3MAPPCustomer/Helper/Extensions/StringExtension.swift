//
//  StringExtension.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

extension String
{
    func validURL() -> String
    {
        if self.hasPrefix("http://") || self.hasPrefix("https://") {
            return self
        }
        else{
            return "http://"+self
        }
    }
    
    func isValidEmail() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPhoneNumber() -> Bool {
        guard self.count > 0 else { return false }
        let phoneRegEx = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: self.trimString())
    }
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
        
    }
    
    func trimString() -> String{
        
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        let fontSize = UIFont.systemFontSize
        let attrs = [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: fontSize),
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        let nonBoldAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: fontSize),
            ]
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
        if let range = nonBoldRange {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        return attrStr
    }
    /*
    func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
                       length: utf16.distance(from: from, to: to))
    }
    */
    
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location + nsRange.length, limitedBy: utf16.endIndex),
            let from = from16.samePosition(in: self),
            let to = to16.samePosition(in: self)
            else { return nil }
        return from ..< to
    }
    
    func changeDate(_ mydate:String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let convertedDate = dateFormatter.date(from: mydate)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.string(from: convertedDate!)
        return date
    }
    
    func getNumberOfDaysFromDate(dateString:String)->String
    {
        //2017-07-03T12:27:35.17
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS"
        let convertedDate = dateFormater.date(from: dateString)
        
        guard convertedDate != nil else {return "Now"}
        
        let calendar = Calendar.current
        let now = Date()
        let ageComponents = calendar.dateComponents([.day], from: convertedDate!, to: now as Date)
        let day = ageComponents.day!
        print(day)
        
        if day==0{
            return "Today"
        }
        return "\(day)d"
        
    }
    
    // MARK:- String class extension for capitalizing first character
    func capitalizingFirstLetter() -> String
    {
        let first = String(prefix(1)).capitalized
        let other = String(dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var html2AttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            let attributedText = try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            attributedText.convertFontTo(font: UIFont.systemFont(ofSize: 16))
            return  attributedText
            //            return try NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    
    
    
}

extension NSMutableAttributedString
{
    
    //let attributedText = try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
    func convertFontTo(font: UIFont)
    {
        var range = NSMakeRange(0, 0)
        
        while (NSMaxRange(range) < length)
        {
            let attribute = attributes(at: NSMaxRange(range), effectiveRange: &range)
            if let oldFont = attribute[NSAttributedString.Key.font]
            {
                let newFont = UIFont(descriptor: font.fontDescriptor.withSymbolicTraits((oldFont as AnyObject).fontDescriptor.symbolicTraits)!, size: font.pointSize)
                addAttribute(NSAttributedString.Key.font, value: newFont, range: range)
            }
        }
    }
}

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    public func makeURL() -> URL? {
        
        let trimmed = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        guard let url = URL(string: trimmed ?? "") else {
            return nil
        }
        return url
    }}

