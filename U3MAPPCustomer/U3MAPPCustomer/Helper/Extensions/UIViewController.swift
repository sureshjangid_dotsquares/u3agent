//
//  File.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 10/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

extension UIViewController:UIGestureRecognizerDelegate,UINavigationControllerDelegate
{
    func didTapBackBtn(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideNavigationBar()  {
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavigationBar()  {
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func initializeFooterView() -> UIView{
        let footerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 40.0))
        let actInd = UIActivityIndicatorView(style: .gray)
        actInd.tag = 10
        actInd.center = CGPoint(x: footerView.bounds.midX, y: footerView.bounds.midY)
        actInd.hidesWhenStopped = true
        footerView.addSubview(actInd)
        return footerView
    }
    
    func initializeHeaderView() -> UIView{
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 40.0))
        let actInd = UIActivityIndicatorView(style: .gray)
        actInd.tag = 10
        actInd.center = CGPoint(x: headerView.bounds.midX, y: headerView.bounds.midY)
        actInd.hidesWhenStopped = true
        headerView.addSubview(actInd)
        return headerView
    }
    
    func presentAlertWith(message:String)
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
            
            let ok=UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func presentAlertWith(message:String,okaction:@escaping (()->Void))
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
            
            let ok=UIAlertAction(title: "OK", style: .default) { (action) in
                okaction()
            }
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    func presentAlertWith(message:String,oKTitle:String,okaction:@escaping (()->Void),cancelAction:@escaping (()->Void))
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
            
            let ok=UIAlertAction(title: oKTitle, style: .default) { (action) in
                okaction()
            }
            let cancel=UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                cancelAction()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func presentAlertWith(message:String,oKTitle:String,cancelTitle:String,okaction:@escaping (()->Void),cancelAction:@escaping (()->Void))
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
            
            let ok=UIAlertAction(title: oKTitle, style: .default) { (action) in
                okaction()
            }
            let cancel=UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
                cancelAction()
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func presentActionTwoWith(title:String,message:String, firstAction:String, FAction:@escaping (()->Void), secondAction:String, SAction:@escaping (()->Void),cancelTitle:String)
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            
            let f=UIAlertAction(title: firstAction, style: .default) { (action) in
                FAction()
            }
            let s=UIAlertAction(title: secondAction, style: .default) { (action) in
                SAction()
            }
            let cancel=UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
                
            }
            
            alert.addAction(f)
            alert.addAction(s)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func presentActionThreeWith(title:String,message:String, firstAction:String, FAction:@escaping (()->Void), secondAction:String, SAction:@escaping (()->Void), thirdAction:String, ThirdAction:@escaping (()->Void),cancelTitle:String)
    {
        let systemVersion : NSString  = UIDevice.current.systemVersion as NSString
        if systemVersion.floatValue >= 8.0
        {
            let alert=UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            
            let f=UIAlertAction(title: firstAction, style: .default) { (action) in
                FAction()
            }
            let s=UIAlertAction(title: secondAction, style: .default) { (action) in
                SAction()
            }
            let t=UIAlertAction(title: thirdAction, style: .default) { (action) in
                ThirdAction()
            }
            let cancel=UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
                
            }
            
            alert.addAction(f)
            alert.addAction(s)
            alert.addAction(t)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func isAddLeftSwipeInNavigation(value:Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = value
    }
    
    // MARK:  Logout Method
    func logoutPressed()
    {
        DispatchQueue.main.async {
            
            self.presentAlertWith(message: "Are you sure you want to logout?", oKTitle: "Yes", okaction: {
                APPDELEGATE.wsLogout { (isSuccess) in
                    if isSuccess {
                        self.logout()
                    }else {
                        self.presentAlertWith(message: "An unexpected error occured while logout, please try again later")
                    }
                }
            }) {}
            
        }
    }
    
    // MARK: - logout from application
    
    func logout(){
        
        //clear user data
        
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "isLoggedIn")
        defaults.removeObject(forKey: "isProfileCompleted")
        defaults.removeObject(forKey: "isRememberMe")
        defaults.removeObject(forKey: "UserModel")
        defaults.synchronize()
        AppManager.shared.showRootView()
    }

}

extension UIViewController {
  
 /**
  Checks whether controller can perform specific segue or not.
  - parameter identifier: Identifier of UIStoryboardSegue.
  */
 func canPerformSegue(withIdentifier identifier: String) -> Bool {
   //first fetch segue templates set in storyboard.
   guard let identifiers = value(forKey: "storyboardSegueTemplates") as? [NSObject] else {
     //if cannot fetch, return false
      return false
   }
   //check every object in segue templates, if it has a value for key _identifier equals your identifier.
    let canPerform = identifiers.contains { (object) -> Bool in
      if let id = object.value(forKey: "_identifier") as? String {
        if id == identifier{
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    }
    return canPerform
  }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}



typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}

extension UIView {
    
    func applyGradient(with colours: [UIColor], locations: [NSNumber]? = nil) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(with colours: [UIColor], gradient orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradient(colors: [CGColor], angle: Float = 0) {
        let gradientLayerView: UIView = UIView(frame: CGRect(x:0, y: 0, width: bounds.width, height: bounds.height))
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = gradientLayerView.bounds
        gradient.colors = colors

        let alpha: Float = angle / 360
        let startPointX = powf(
            sinf(2 * Float.pi * ((alpha + 0.75) / 2)),
            2
        )
        let startPointY = powf(
            sinf(2 * Float.pi * ((alpha + 0) / 2)),
            2
        )
        let endPointX = powf(
            sinf(2 * Float.pi * ((alpha + 0.25) / 2)),
            2
        )
        let endPointY = powf(
            sinf(2 * Float.pi * ((alpha + 0.5) / 2)),
            2
        )

        gradient.endPoint = CGPoint(x: CGFloat(endPointX),y: CGFloat(endPointY))
        gradient.startPoint = CGPoint(x: CGFloat(startPointX), y: CGFloat(startPointY))

        gradientLayerView.layer.insertSublayer(gradient, at: 0)
        layer.insertSublayer(gradientLayerView.layer, at: 0)
    }
}
