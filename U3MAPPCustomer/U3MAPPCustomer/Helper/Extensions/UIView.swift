//
//  UIView.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

public extension UIView {
    
    var width:      CGFloat { return self.frame.size.width }
     var height:     CGFloat { return self.frame.size.height }
     var size:       CGSize  { return self.frame.size}
    
     var origin:     CGPoint { return self.frame.origin }
     var x:          CGFloat { return self.frame.origin.x }
     var y:          CGFloat { return self.frame.origin.y }
     var centerX:    CGFloat { return self.center.x }
     var centerY:    CGFloat { return self.center.y }
    
     var left:       CGFloat { return self.frame.origin.x }
     var right:      CGFloat { return self.frame.origin.x + self.frame.size.width }
     var top:        CGFloat { return self.frame.origin.y }
     var bottom:     CGFloat { return self.frame.origin.y + self.frame.size.height }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var shadowColor:  UIColor? {
        set {
//            layer.masksToBounds =  false
//            layer.shadowColor = newValue?.cgColor
//            layer.shadowRadius = 2.0
//            layer.shadowOffset = CGSize(width:0, height:2)
//            layer.shadowOpacity = 1.0
            
            layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
            layer.shadowOffset = CGSize(width:0, height:5)
            layer.shadowOpacity = 1.0
            layer.shadowRadius = 2.0
        }
        get {
            guard let color = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }

}

extension Int {
    var boolValue: Bool { return self != 0 }
}
