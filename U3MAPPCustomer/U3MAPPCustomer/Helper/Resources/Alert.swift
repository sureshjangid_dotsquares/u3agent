//
//  Alert.swift
//  Generic
//
//  Created by Vinod Kumar on 26/09/17.
//  Copyright © 2017 Vinod Kumar. All rights reserved.
//

import UIKit

//MARK:- alert show
//**********---------- Showing alert in app (Generic) ----------**********
func showAlert(title: String = APPLICATION_NAME, message: String) -> Void {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            topController.present(alert, animated: true)
        }
    }
}

