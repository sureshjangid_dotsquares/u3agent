
import UIKit

public extension UIImage {
  func normalizeOrientation() -> UIImage {
    guard imageOrientation != .up else { return self }
    UIGraphicsBeginImageContext(size)
    draw(in: CGRect(origin: .zero, size: size))
    let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return normalizedImage ?? self
  }
}
